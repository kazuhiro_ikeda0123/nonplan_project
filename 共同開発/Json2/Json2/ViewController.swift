//
//  ViewController.swift
//  Json2
//
//  Created by ikeda on 2020/08/23.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    @IBOutlet weak var jsonLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func startButton(_ sender: Any) {
        
        Alamofire.request("https://pixabay.com/api/?key=17617424-ce248788983e8785377ef6493&q=jojo").responseJSON {
                   res in
                   if res.result.isSuccess {
                       if let returnValue = res.result.value {
                        print(JSON(returnValue)["hits"][0]["tags"])
                        self.jsonLabel.text = JSON(returnValue)["hits"][0]["tags"].string!
                       }
                   } else {
                       print("Error!")
                   }
               }
        
        
           }
    
}

