//
//  DispPlayerViewController.swift
//  wordwolf
//
//  Created by ikeda on 2020/07/23.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class DispPlayerViewController: UIViewController {

    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var word1: UILabel!
    
    var nameArray:[String] = []
    var nameIndex = 0
    var buttonCounter = 0
    
    var wordArray:[String] = []
    var wordIndex = 0

    var choice:[String] = []
    
    var nameAndWordArray:[[String]] = []
    
    var nextControllerCounter = 0
    
    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        okButton.layer.cornerRadius = 20.0
        
        nameArray = UserDefaults.standard.stringArray(forKey: "nameArrayKey")!
        wordArray = UserDefaults.standard.stringArray(forKey: "wordArrayKey")!

        wordShuffule()

        name1.text = nameAndWordArray[nameIndex][0]
        word1.text = "あたなは" + nameAndWordArray[nameIndex][0] +  "さんですか？"

    }
    
    @IBAction func OKButton(_ sender: Any) {
        
        if (nextControllerCounter != nameAndWordArray.count) {
            buttonCounter += 1
            if buttonCounter >= 2{
                nameIndex += 1
                name1.text = nameAndWordArray[nameIndex][0]
                word1.text = "あなたは" + nameAndWordArray[nameIndex][0] +  "さんですか？"
                buttonCounter = 0
            } else if buttonCounter >= 1 {
                word1.text = "あなたのワードは" + nameAndWordArray[nameIndex][1] + "です"
                nextControllerCounter += 1
            }
        } else {
        
            //決定したnameとwordの組み合わせを記憶
            UserDefaults.standard.set(nameAndWordArray, forKey: "resultKey")
            
            // ①storyboardのインスタンス取得
            let storyboard: UIStoryboard = self.storyboard!

            // ②遷移先ViewControllerのインスタンス取得
            let nextView = storyboard.instantiateViewController(withIdentifier: "view2") as! View2ViewController

            // ③画面遷移
            self.present(nextView, animated: true, completion: nil)
        
        }
        
    }
    
    func wordShuffule () {
        wordArray.shuffle()
        
        for count in 0...nameArray.count - 1 {
            
            if count == 0 {
                
                choice.append(wordArray[0])
                
            }else{
                
                choice.append(wordArray[1])
                
            }
        
        }
        
        choice.shuffle()
        
        for count in 0...nameArray.count - 1 {
            
            nameAndWordArray.append([nameArray[count],choice[count]])
        }
        
    }
    
}
