//
//  WordViewController.swift
//  wordwolf
//
//  Created by ikeda on 2020/07/23.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class WordViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var wordArray:[String] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var textWord: UITextField!
    
    @IBOutlet weak var returnButton: UIButton!
    
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        returnButton.layer.cornerRadius = 20.0
        addButton.layer.cornerRadius = 20.0
        
        // tableView の dataSource 問い合わせ先を self に
        tableView.dataSource = self
        
        // tableView の delegate 問い合わせ先を self に
        tableView.delegate = self
        
        if UserDefaults.standard.object(forKey: "wordArrayKey") != nil {
            wordArray = UserDefaults.standard.stringArray(forKey: "wordArrayKey")!
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wordArray.count
    }
       
    //selectwordの配列の中身を返す
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
    // セルを取得
    let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
    //セルに表示する値を取得する
    cell.textLabel!.text = wordArray[indexPath.row]
    return cell
    
    }
    
    // ボタン押下でワードを登録して
    // TableViewを洗替てNilクリア
    @IBAction func tsuika(_ sender: Any) {
        
        if textWord.text != "" {
            
            let strWord = textWord.text!
            wordArray.append(strWord)
            textWord.text = ""
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        // Cellスライドで配列削除
        if editingStyle == UITableViewCell.EditingStyle.delete {
            wordArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        
        // 戻るボタン押下時に登録した配列を記録
        UserDefaults.standard.set(wordArray, forKey: "wordArrayKey")
        
    }
    
}
