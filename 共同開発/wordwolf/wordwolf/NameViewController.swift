//
//  NameViewController.swift
//  wordwolf
//
//  Created by ikeda on 2020/07/23.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class NameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var nameArray:[String] = []
    
    @IBOutlet weak var tableName: UITableView!
    
    @IBOutlet weak var nameText: UITextField!

    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton.layer.cornerRadius = 20.0
        addButton.layer.cornerRadius = 20.0

        // tableView の dataSource 問い合わせ先を self に
        tableName.dataSource = self
        
        // tableView の delegate 問い合わせ先を self に
        tableName.delegate = self
        
        if UserDefaults.standard.object(forKey: "nameArrayKey") != nil {
            nameArray = UserDefaults.standard.stringArray(forKey: "nameArrayKey")!
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // セルを取得
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
               
        //セルに表示する値を取得する
        cell.textLabel!.text = nameArray[indexPath.row]
        return cell
    }
    
    @IBAction func namebuttom(_ sender: Any) {
        
        if nameText.text != ""{
        
            let strName = nameText.text!
            nameArray.append(strName)
            nameText.text = ""
            tableName.reloadData()
        }
    }
    
    func nameView(_ nameView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
           return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
       // Cellスライドで配列削除
       if editingStyle == UITableViewCell.EditingStyle.delete {
           nameArray.remove(at: indexPath.row)
           tableView.deleteRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.automatic)
       }
    }
       
    
    @IBAction func nextButton(_ sender: Any) {
        
        // 戻るボタン押下時に登録した配列を記録
        UserDefaults.standard.set(nameArray, forKey: "nameArrayKey")
    
    }
    
}
