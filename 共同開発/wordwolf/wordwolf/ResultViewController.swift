//
//  ResultViewController.swift
//  wordwolf
//
//  Created by ikeda on 2020/08/01.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var result: UITableView!
    @IBOutlet weak var topButton: UIButton!
    
    var resultArray:[[String]] = []
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topButton.layer.cornerRadius = 20.0
        
        resultArray = UserDefaults.standard.array(forKey: "resultKey") as! [[String]]
        
        result.delegate = self
        result.dataSource = self
        
    }
    
    //セルの数を指定
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultArray.count
    }

    //セルに値を設定
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // セルを取得する
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        // セルに表示する値を設定する
        cell.textLabel!.text = resultArray[indexPath.row][0] + "さんのワードは " + resultArray[indexPath.row][1]
        
        return cell

    }

    @IBAction func top(_ sender: Any) {
        
        // ①storyboardのインスタンス取得
        let storyboard: UIStoryboard = self.storyboard!

        //②遷移先ViewControllerのインスタンス取得
        let topView = storyboard.instantiateViewController(withIdentifier: "topview") as! ViewController

        // ③画面遷移
        self.present(topView, animated: true, completion: nil)

    }
    
}
