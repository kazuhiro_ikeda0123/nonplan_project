//
//  ViewController.swift
//  wordwolf
//
//  Created by 池田一篤 on 2020/07/23.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var word: UIButton!
    
    @IBOutlet weak var playerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        word.layer.cornerRadius = 20.0
        playerButton.layer.cornerRadius = 20.0
        
    }
    
    
    

}

