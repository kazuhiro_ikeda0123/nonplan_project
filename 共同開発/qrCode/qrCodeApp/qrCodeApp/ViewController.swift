//
//  ViewController.swift
//  qrCodeApp
//
//  Created by 池田一篤 on 2020/08/08.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var word: UITextField!
    
    @IBOutlet weak var display: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
       }
    
    @IBAction func button(_ sender: Any) {
        
        display.text = word.text
        
        if display.text == "" {
            
            print("未入力")
            
        } else {
            
            //文字列をNSDataに変換し、QRコードを作成します。
            //Converts a string to NSData.
            let str = display.text
            let data = str!.data(using: String.Encoding.utf8)!
           
            //URLをNSDataに変換し、QRコードを作成します。
            //Converts a url to NSData.
            //let url = "http://swiswiswift.com"
            //let data = url.data(using: String.Encoding.utf8)!

            //QRコードを生成します。
            //Generate QR code.
            let qr = CIFilter(name: "CIQRCodeGenerator", parameters: ["inputMessage": data, "inputCorrectionLevel": "M"])!
            let sizeTransform = CGAffineTransform(scaleX: 5, y: 5)
            let qrImage = qr.outputImage!.transformed(by: sizeTransform)
            let context = CIContext()
            let cgImage = context.createCGImage(qrImage, from: qrImage.extent)
            let uiImage = UIImage(cgImage: cgImage!)

            //作成したQRコードを表示します
            //Display QR code
            let qrImageView = UIImageView()
            qrImageView.contentMode = .scaleAspectFit
            qrImageView.frame = self.view.frame
            qrImageView.image = uiImage
            self.view.addSubview(qrImageView)

        }

    }
    
}
