//
//  HistoryViewController.swift
//  MuscleTraining
//
//  Created by 池田一篤 on 2020/09/06.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTableView: UITableView!
    
    //  空の配列を用意
    var strArray:[Str] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // `JSONDecoder` で `Data` 型を自作した構造体へデコードする
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let data = UserDefaults.standard.data(forKey: "SaveKey"),
            let str = try? jsonDecoder.decode(Str.self, from: data) else {
                return
        }
        
        // 初回登録の場合カスタム配列にアペンドして記録
        // 既存の場合上記で用意したカスタム配列へ呼び起こし
        if (UserDefaults.standard.array(forKey: "StrArrayKey") != nil) {
            strArray = readItems()!
        }
        
        strArray.append(str)
        saveItems(items: strArray)

        myTableView.dataSource = self
        myTableView.delegate = self
    
    }
    
    func saveItems(items: [Str]) {
        let data = items.map { try! JSONEncoder().encode($0) }
        UserDefaults.standard.set(data as [Any], forKey: "StrArrayKey")
    }
    
    func readItems() -> [Str]? {
        guard let items = UserDefaults.standard.array(forKey: "StrArrayKey") as? [Data] else { return [Str]() }

        let decodedItems = items.map { try! JSONDecoder().decode(Str.self, from: $0) }
        return decodedItems
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return strArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myTableView.dequeueReusableCell(withIdentifier: "MyTableViewCell", for: indexPath)
        
        cell.textLabel?.text = strArray[indexPath.row].year + "年" + strArray[indexPath.row].month + "月" + strArray[indexPath.row].day + "日"
          
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
       // セルの選択を解除
       tableView.deselectRow(at: indexPath, animated: true)
            
        let recordViewController = self.storyboard?.instantiateViewController(withIdentifier: "record") as! RecordViewController
        self.present(recordViewController, animated: true, completion: nil)
        
       }


}
