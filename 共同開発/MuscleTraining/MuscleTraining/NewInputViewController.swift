//
//  NewInputViewController.swift
//  MuscleTraining
//
//  Created by ikeda on 2020/09/06.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class NewInputViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var armTextField: UITextField!
    @IBOutlet weak var bellyTextField: UITextField!
    @IBOutlet weak var spineTextField: UITextField!
    @IBOutlet weak var squatTextField: UITextField!
    @IBOutlet weak var plankTextField: UITextField!
    
    var strsData:[Str] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
        var yearText = yearTextField.text!
        var monthText = monthTextField.text!
        var dayText = dayTextField.text!
        
        
        let storyboard: UIStoryboard = self.storyboard!
        let second = storyboard.instantiateViewController(withIdentifier: "history")
        self.navigationController?.pushViewController(second, animated: true)
        
        let str = Str(year: yearText, month: monthText, day: dayText, arm: "20", belly: "100", spine: "300", squat: "4000", plank: "6000")

        // `JSONEncoder` で `Data` 型へエンコードし、UserDefaultsに追加する
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = .convertToSnakeCase
        guard let data = try? jsonEncoder.encode(str) else {
            return
        }
        UserDefaults.standard.set(data, forKey: "SaveKey")
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
