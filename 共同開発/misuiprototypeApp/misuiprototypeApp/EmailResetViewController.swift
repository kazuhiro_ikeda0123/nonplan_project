//
//  EmailResetViewController.swift
//  misuiprototypeApp
//
//  Created by ikeda on 2020/09/12.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class EmailResetViewController: UIViewController {

    @IBOutlet weak var resetEmailTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "パスワードリセット"
        
        let resetEmailBorder = CALayer()
        let resetEmailWidth = CGFloat(2.0)
        
        resetEmailBorder.borderColor = UIColor.gray.cgColor
        resetEmailBorder.frame = CGRect(x: 0, y: resetEmailTextField.frame.size.height - resetEmailWidth, width:  resetEmailTextField.frame.size.width, height: 1)
         resetEmailBorder.borderWidth = resetEmailWidth
        
        // テキストフィールドの下線設定
        resetEmailTextField.borderStyle = .none
        resetEmailTextField.layer.addSublayer(resetEmailBorder)
 
        // ログインボタンに丸みを持たせる
        sendButton.layer.cornerRadius = 10.0
            
    }

}
