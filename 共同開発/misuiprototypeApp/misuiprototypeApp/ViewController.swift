//
//  ViewController.swift
//  misuiprototypeApp
//
//  Created by 池田一篤 on 2020/09/12.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "巳翠プロトタイプ"
        
        // ボーダー準備
        let emailBorder = CALayer()
        let emailWidth = CGFloat(2.0)
        emailBorder.borderColor = UIColor.gray.cgColor
        emailBorder.frame = CGRect(x: 0, y: emailTextField.frame.size.height - emailWidth, width:  emailTextField.frame.size.width, height: 1)
        emailBorder.borderWidth = emailWidth

        let passWordBorder = CALayer()
        let passWordWidth = CGFloat(2.0)
        passWordBorder.borderColor = UIColor.gray.cgColor
        passWordBorder.frame = CGRect(x: 0, y: passWordTextField.frame.size.height - passWordWidth, width:  passWordTextField.frame.size.width, height: 1)
        passWordBorder.borderWidth = passWordWidth
        
        // テキストフィールドの下線設定
        emailTextField.borderStyle = .none
        emailTextField.layer.addSublayer(emailBorder)
        
        passWordTextField.borderStyle = .none
        passWordTextField.layer.addSublayer(passWordBorder)
        
        
        // ログインボタンに丸みを持たせる
        loginButton.layer.cornerRadius = 10.0
        
        // 次の画面のBackボタンを「戻る」に変更
       self.navigationItem.backBarButtonItem = UIBarButtonItem(
           title:  "ログイン",
           style:  .plain,
           target: nil,
           action: nil
       )
        
        
        
    }

}

