//
//  ScanViewController.swift
//  misuiprototypeApp
//
//  Created by ikeda on 2020/09/12.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit
import AVFoundation

class ScanViewController: UIViewController {
    
    let myQRCodeReader = MyQRCodeReader()
        
    override func viewDidLoad() {
        super.viewDidLoad()
                
//        myQRCodeReader.delegate = self
//        myQRCodeReader.setupCamera(view:self.view)
//        //読み込めるカメラ範囲
//        myQRCodeReader.readRange()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.title = "スキャン１枚目"
        self.tabBarController?.navigationItem.hidesBackButton = true

    }
    
}

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate{
    //対象を認識、読み込んだ時に呼ばれる
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //一画面上に複数のQRがある場合、複数読み込むが今回は便宜的に先頭のオブジェクトを処理
        if let metadata = metadataObjects.first as? AVMetadataMachineReadableCodeObject{
            let barCode = myQRCodeReader.previewLayer.transformedMetadataObject(for: metadata) as! AVMetadataMachineReadableCodeObject
            //読み込んだQRを映像上で枠を囲む。ユーザへの通知。必要な時は記述しなくてよい。
            myQRCodeReader.qrView.frame = barCode.bounds
            //QRデータを表示
            if let str = metadata.stringValue {
                
                //アラート生成
                //UIAlertControllerのスタイルがalert
                let alert: UIAlertController = UIAlertController(title: "読み込み油種", message:  "こちらは" + str + "です", preferredStyle:  UIAlertController.Style.alert)
                // 確定ボタンの処理
                let confirmAction: UIAlertAction = UIAlertAction(title: "確定", style: UIAlertAction.Style.default, handler:{
                    // 確定ボタンが押された時の処理をクロージャ実装する
                    (action: UIAlertAction!) -> Void in
                    //実際の処理
                    print("スキャン２枚目を行う")
                    
                })
                // キャンセルボタンの処理
                let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
                    // キャンセルボタンが押された時の処理をクロージャ実装する
                    (action: UIAlertAction!) -> Void in
                    //実際の処理
                    print("キャンセル")
                })
                
                //UIAlertControllerにキャンセルボタンと確定ボタンをActionを追加
                alert.addAction(cancelAction)
                alert.addAction(confirmAction)

                //実際にAlertを表示する
                present(alert, animated: true, completion: nil)

            }
        }
    }
}
