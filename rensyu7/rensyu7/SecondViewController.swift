//
//  SecondViewController.swift
//  rensyu7
//
//  Created by ikeda on 2020/06/20.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    
    
    @IBOutlet weak var anser: UILabel!
    
    @IBOutlet weak var box1: UITextField!
    
    @IBOutlet weak var box2: UITextField!
    
    override func viewDidLoad() {
        
        anser.text = "0"
        
        super.viewDidLoad()

    
    }

    @IBAction func button(_ sender: Any) {

        var nbox1:Int = Int(box1.text!)!
        var nbox2:Int = Int(box2.text!)!
        
        anser.text = String(nbox1 * nbox2)
        
        
    }
    


}
