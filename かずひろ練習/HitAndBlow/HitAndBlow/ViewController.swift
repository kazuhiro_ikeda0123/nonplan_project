//
//  ViewController.swift
//  HitAndBlow
//
//  Created by 池田一篤 on 2020/06/25.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{

    var ansArray:[String] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var anserTextField: UITextField!
    @IBOutlet weak var hitTextField: UITextField!
    @IBOutlet weak var blowTextField: UITextField!
    
    var hit:String = ""
    var blow:String = ""
    
    var ans1:String = ""
    var ans2:String = ""
    var ans3:String = ""
    var j = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // 初期数字を設定するFunc呼び出し
        makeArrayList()
        
        // Do any additional setup after loading the view.
    }
    
    // 初期数字の設定（重複数値除外）
    func makeArrayList () {
        
        var strI = ""
        
        var num1:String
        var num2:String
        var num3:String
        
        for i in 0...999 {
            
            // カウンターiを３桁０埋め
            strI = String(format: "%03d", i)
            
            // ３桁の数字を１桁目 ２桁目 ３桁目 で切り出し
            num1 = String(strI.prefix(1))
            let startIndex = strI.index(strI.startIndex, offsetBy: 1)
            let endIndex = strI.index(startIndex, offsetBy: 1)
            num2 = String(strI[startIndex..<endIndex])
            num3 = String (strI.suffix(1))
            
            // 重複数字が存在しない場合のみ配列登録
            if num1 != num2 && num2 != num3 && num1 != num3 {
                ansArray.append(strI)
            }
            
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ansArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = ansArray[indexPath.row]
        
        return cell
    }
    
    @IBAction func calcAction(_ sender: Any) {
        
        let anserNumber = anserTextField.text!
        hit = hitTextField.text!
        blow = blowTextField.text!
        
        // ３桁の数字を１桁目 ２桁目 ３桁目 で切り出し
        ans1 = String(anserNumber.prefix(1))
        let startIndex = anserNumber.index(anserNumber.startIndex, offsetBy: 1)
        let endIndex = anserNumber.index(startIndex, offsetBy: 1)
        ans2 = String(anserNumber[startIndex..<endIndex])
        ans3 = String(anserNumber.suffix(1))
        
        hitAndBlowCullc()
        
    }
    
    func ansArrayRemove () {
        ansArray.remove(at: j)
        j -= 1
    }
    
    func hitAndBlowCullc () {
        
        var num1:String
        var num2:String
        var num3:String

        // for ループでiをデクリメントできないのでWhile
        j = 0
        while j < ansArray.count {
            // ３桁の数字を１桁目 ２桁目 ３桁目 で切り出し
            num1 = String (ansArray[j].prefix(1))
            let startIndex = ansArray[j].index(ansArray[j].startIndex, offsetBy: 1)
            let endIndex = ansArray[j].index(startIndex, offsetBy: 1)
            num2 = String (ansArray[j][startIndex..<endIndex])
            num3 = String (ansArray[j].suffix(1))
            
            // 0H 0B
            if hit == "0" && blow == "0" {
                
                if ans1 == num1 || ans1 == num2 || ans1 == num3 || ans2 == num1 || ans2 == num2 || ans2 == num3 || ans3 == num1 || ans3 == num2 || ans3 == num3 {
                    ansArrayRemove()
                }
            // 0H 1B
            } else if hit == "0" && blow == "1" {
                
                if (ans1 == num1 || ans2 == num2 || ans3 == num3)
                || ((ans1 != num2 && ans1 != num3) && (ans2 != num1 && ans2 != num3) && (ans3 != num1 && ans3 != num2))
                || ((ans1 == num2 && (ans2 == num1 || ans2 == num3)) || (ans1 == num2 && (ans3 == num1) || ans3 == num2)
                || (ans2 == num1 && (ans1 == num2 || ans1 == num3)) || (ans2 == num1 && (ans3 == num1 || ans3 == num2))
                || (ans3 == num1 && (ans1 == num2 || ans1 == num3)) || (ans3 == num1 && (ans2 == num1 || ans2 == num3))){
                    ansArrayRemove()
                }
            // 0H 2B
            } else if hit == "0" && blow == "2" {
                if (ans1 == num1 || ans2 == num2 || ans3 == num3)
                || ((ans1 == num2 || ans1 == num3) && ((ans2 != num1 && ans2 != num3) && (ans3 != num1 && ans3 != num2)))
                || ((ans2 == num1 || ans2 == num3) && ((ans1 != num2 && ans1 != num3) && (ans3 != num1 && ans3 != num2)))
                || ((ans3 == num2 || ans3 == num1) && ((ans1 != num2 && ans1 != num3) && (ans2 != num1 && ans2 != num3)))
                || (((ans1 != num2 && ans1 != num3) && (ans2 != num1 && ans2 != num3) && (ans3 != num1 && ans3 != num2))){
                    ansArrayRemove()
                }
            // 0H 3B
            } else if hit == "0" && blow == "3" {
                if !((ans1 == num2) && (ans3 == num3) && (ans2 == num1))
                && !((ans1 == num2) && (ans2 == num3) && (ans3 == num1))
                && !((ans1 == num3) && (ans2 == num1) && (ans3 == num1))
                && !((ans1 == num3) && (ans2 == num1) && (ans3 == num2))
                && !((ans1 == num3) && (ans2 == num2) && (ans3 == num1))
                && !((ans1 == num1) && (ans2 == num3) && (ans3 == num2)){
                    ansArrayRemove()
                }
            // 0H 0B と 0H 1B2B3B が除外されていない
            // 1H 0B
            } else if hit == "1" && blow == "0" {
                if !(ans1 == num1) && !(ans2 == num2) && !(ans3 == num3){
                    ansArrayRemove()
                }
            // 2H 0B
            } else if hit == "2" && blow == "0" {
                if (ans1 != num1 && ans2 != num2)
                || (ans1 != num1 && ans3 != num3)
                || (ans2 != num2 && ans3 != num3){
                    ansArrayRemove()
                }
            // 1H 1B
            } else if hit == "1" && blow == "1" {
                if !((ans1 == num1) && (ans2 == num3))
                && !((ans1 == num1) && (ans3 == num2))
                && !((ans2 == num2) && (ans1 == num3))
                && !((ans2 == num2) && (ans3 == num1))
                && !((ans3 == num3) && (ans1 == num2))
                && !((ans3 == num3) && (ans2 == num1)){
                    ansArrayRemove()
                }
            // 1H 2B
            } else if hit == "1" && blow == "2" {
                if !(ans1 == num1 && ((ans2 == num3) && (ans3 == num2)))
                && !(ans2 == num2 && ((ans1 == num3) && (ans3 == num1)))
                && !(ans3 == num3 && ((ans1 == num2) && (ans2 == num1))){
                    ansArrayRemove()
                }
            }
            j += 1
            tableView.reloadData()

        }
    }
}

