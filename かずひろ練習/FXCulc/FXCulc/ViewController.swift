//
//  ViewController.swift
//  FXCulc
//
//  Created by 池田一篤 on 2020/09/06.
//  Copyright © 2020 池田一篤. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var shikinTextField: UITextField!
    
    @IBOutlet weak var lotTextField: UITextField!
    
    @IBOutlet weak var riskTextField: UITextField!
    
    @IBOutlet weak var rossCutRate: UILabel!
    
    @IBOutlet weak var rickMoneyLabel: UILabel!
    
    @IBOutlet weak var entryRate: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func buttonAction(_ sender: Any) {
        
        var shikinDouble = Double(shikinTextField.text!)!
        
        var rotInt = Int(lotTextField.text!)! * 1000
        
        var riskDouble = Double(riskTextField.text!)! / 100
        
        rickMoneyLabel.text = String(shikinDouble * riskDouble)
        
        var entryRateDouble = Double(entryRate.text!)!
        
        rossCutRate.text = String(entryRateDouble - riskDouble * 10)
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

