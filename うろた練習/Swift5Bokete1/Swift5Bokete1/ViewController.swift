//
//  ViewController.swift
//  Swift5Bokete1
//
//  Created by ikeda on 2020/07/13.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import Photos

class ViewController: UIViewController {

    @IBOutlet weak var odaiImageView: UIImageView!
    
    @IBOutlet weak var comentTextView: UITextView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        comentTextView.layer.cornerRadius = 20.0
        
        PHPhotoLibrary.requestAuthorization {(status) in
            
            switch(status){
            case .authorized: break
            case .denied: break
            case .notDetermined: break
            case .restricted: break
            }
        }
        
        getImages(keyword: "funny")
        
    }
        //検索キーワードの値を元に画像を引っ張ってくる
        //pixabay.com
    func getImages(keyword:String){
        
        //APIkey 17617424-ce248788983e8785377ef6493
        let url = "https://pixabay.com/api/?key=17617424-ce248788983e8785377ef6493&q=\(keyword)"
        
        //Alamofireを使ってhttpリクエストを投げます。
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON{ (respons) in
            
            switch respons.result{
                
            case .success:
                
                let json:JSON = JSON(respons.data as Any)
                var imageString = json["hits"][self.count]["webformatURL"].string
                
                if imageString == nil{
                    
                    imageString = json["hits"][0]["webformatURL"].string
                    self.odaiImageView.sd_setImage(with: URL(string: imageString!), completed: nil)
                    
                }else{
                    
                    self.odaiImageView.sd_setImage(with: URL(string: imageString!), completed: nil)
                    
                }
        
            case .failure(let error): break
                
            }
        
        }
        
    }
        
    @IBAction func nextOdai(_ sender: Any) {
        
        count = count + 1
        
        if searchTextField.text == ""{
            
            getImages(keyword: "funny")
            
        }else{
            
            getImages(keyword: searchTextField.text!)
            
        }
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        self.count = 0
        
        if searchTextField.text == ""{
                   
                   getImages(keyword: "funny")
                   
        }else{
                   
                   getImages(keyword: searchTextField.text!)
            
           }

    }

    @IBAction func next(_ sender: Any) {
        
        performSegue(withIdentifier: "next", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let shareVC = segue.destination as?
            ShareViewController
        shareVC?.commentString = comentTextView.text
        shareVC?.resultImage = odaiImageView.image!
        
    }
    
}

