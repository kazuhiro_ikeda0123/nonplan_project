//
//  ViewController.swift
//  Swift5 Keyboard1
//
//  Created by ikeda on 2020/06/22.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    
    // 行間は見返した時に読みやすいようにするとよし
    @IBOutlet weak var logoimageView: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
   
        userNameTextField.delegate = self
        passWordTextField.delegate = self

    }
    
    @IBAction func login(_ sender: Any) {
        
        
        logoimageView.image = UIImage(named: "loginOK")
        
        // 処理のまとまりで行間をとるとわかりやすい
        // また無駄な行間は削除したほうがよい
        userNameLabel.text = userNameTextField.text
        passwordLabel.text = passWordTextField.text
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    
    
    
    
    

}

