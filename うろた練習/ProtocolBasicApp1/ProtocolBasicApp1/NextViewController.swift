//
//  NextViewController.swift
//  ProtocolBasicApp1
//
//  Created by ikeda on 2020/06/27.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

protocol Catchprotocol{
    
    //規則を決める
    func catchData(count:Int)
}

class NextViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    var count = 0
    
    var delegate:Catchprotocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func plus(_ sender: Any) {
    
        count = count + 1
        
        label.text = String(count)
    }
    
    @IBAction func back(_ sender: Any) {
        
        //デリゲートメソッドを任せたクラスで発動させる
        delegate?.catchData(count: count)
        dismiss(animated: true, completion: nil)
    }
}
