//
//  ViewController.swift
//  ProtocolBasicApp1
//
//  Created by ikeda on 2020/06/27.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

//プロトコルを宣言
class ViewController: UIViewController,Catchprotocol {
    
    @IBOutlet weak var label: UILabel!
    
    var count = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    func catchData(count: Int) {
        label.text = String(count)
    }

    @IBAction func next(_ sender: Any) {
        
        performSegue(withIdentifier: "next", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nextVC = segue.destination as! NextViewController
        nextVC.delegate = self
    }
}
