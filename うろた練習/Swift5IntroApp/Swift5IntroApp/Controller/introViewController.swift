//
//  introViewController.swift
//  Swift5IntroApp
//
//  Created by ikeda on 2020/08/24.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import Lottie

class introViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var onboardArray = ["1","2","3","4","5"]
    
    var onbardStringArray = ["test1","test2","test3","test4","test5"]

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.isPagingEnabled = true
        setUpScroll()
        
        for i in 0...4 {
            
            let animationView = AnimationView()
            let animation = Animation.named(onboardArray[i])
            animationView.frame = CGRect(x: CGFloat(i) * view.frame.size.width, y: 0, width: view.frame.size.width, height: view.frame.size.height)
            
            animationView.animation = animation
            animationView.contentMode = .scaleAspectFit
            animationView.loopMode = .loop
            animationView.play()
            scrollView.addSubview(animationView)
            
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    func setUpScroll() {
        
        scrollView.delegate = self
        
        scrollView.contentSize = CGSize(width: view.frame.size.width * 5, height: scrollView.frame.size.height)
        
        for i in 0...4 {
            
            let onbardLabel = UILabel(frame: CGRect(x: CGFloat(i) * view.frame.size.width, y: view.frame.size.height/3, width: scrollView.frame.size.width, height: scrollView.frame.size.height))
            
            onbardLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
            onbardLabel.textAlignment = .center
            onbardLabel.text = onbardStringArray[i]
            scrollView.addSubview(onbardLabel)
            
            
            
            
            
        }
        
        
    }
    
    
    

}
