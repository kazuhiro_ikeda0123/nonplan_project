//
//  Contents.swift
//  Swift5FirebaseAnonymousLogin
//
//  Created by ikeda on 2020/08/09.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import Foundation

class Contents{
    
    var userNameString:String = ""
    var profileImageString:String = ""
    var contentImageString:String = ""
    var comentString:String = ""
    var postDateString:String = ""
    
    init(userNameString:String,profileImageString:String,contentImageString:String,comentString:String,postDateString:String){
        
        self.userNameString = userNameString
        self.profileImageString = profileImageString
        self.contentImageString = contentImageString
        self.comentString = comentString
        self.postDateString = postDateString
        
        
    }
    
    
    
}

