//
//  DetailViewController.swift
//  Swift5FirebaseAnonymousLogin
//
//  Created by ikeda on 2020/08/12.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var userName = String()
    var contentImage = String()
    var date = String()
    var profileImage = String()
    var comment = String()
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var contentsImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        shareButton.layer.cornerRadius = 20.0
        
        profileImageView.sd_setImage(with:URL(string: profileImage) , completed: nil)
        
        
        userNameLabel.text = userName
        
        dateLabel.text = date
        
        contentsImageView.sd_setImage(with:URL(string: contentImage), completed: nil)
        
        commentLabel.text = comment
        
    }
    
    @IBAction func shareAction(_ sender: Any) {
        
        let items = [contentsImageView.image] as Any
        
        let acView = UIActivityViewController(activityItems: [items], applicationActivities: nil)
        
        present(acView, animated: true, completion: nil)
        
    }
    

}
