//
//  inputViewController.swift
//  Swift5FirebaseAnonymousLogin
//
//  Created by ikeda on 2020/08/07.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class inputViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    
    @IBOutlet weak var userNametextField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        logoImageView.layer.cornerRadius = 30.0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
   
    @IBAction func done(_ sender: Any) {
        
        //ユーザー名をアプリ内に保存
        UserDefaults.standard.set(userNametextField.text, forKey: "userName")
        
        //アイコンも保存
        let data = logoImageView.image?.jpegData(compressionQuality: 0.1)
        UserDefaults.standard.set(data, forKey: "userImage")
        
        //画面遷移
        let nextVC = self.storyboard?.instantiateViewController(identifier: "nextVC") as! NextViewController
        
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        userNametextField.resignFirstResponder()
        
    }

    @IBAction func imageViewTap(_ sender: Any) {
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        
        //アラートを出す
        //カメラ　or　アルバムを選択させます
        showAlert()
        
    }
    
    //カメラ立ち上げメソッド
    func doCamera(){
        
        let sourceType:UIImagePickerController.SourceType = .camera
        
        //カメラ利用可能かチェック
        if UIImagePickerController.isSourceTypeAvailable(.camera){
        
        let cameraPicker = UIImagePickerController()
        cameraPicker.allowsEditing = true
        cameraPicker.sourceType = sourceType
        cameraPicker.delegate = self
            self.present(cameraPicker,animated: true,completion: nil)
            
        }
    
    }
    
    func doAlbium(){
           
           let sourceType:UIImagePickerController.SourceType = .photoLibrary
           
           //アルバム利用可能かチェック
           if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
           
           let cameraPicker = UIImagePickerController()
           cameraPicker.allowsEditing = true
           cameraPicker.sourceType = sourceType
           cameraPicker.delegate = self
               self.present(cameraPicker,animated: true,completion: nil)
        
        }
            
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if info[.originalImage]
            as? UIImage != nil{
            
            let selectedImage = info[.originalImage] as! UIImage
                
                UserDefaults.standard.set(selectedImage.jpegData(compressionQuality: 0.1), forKey: "userImage")
                
                logoImageView.image = selectedImage
            
            picker.dismiss(animated: true, completion: nil)
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    
    }
    
    //アラート
    func showAlert(){
    
    let alertController = UIAlertController(title: "選択", message: "どちらを選択しますか？", preferredStyle: .actionSheet)
    
        let action1 = UIAlertAction(title: "カメラ", style: .default){
            (alert) in
            
            self.doCamera()
            
        }
        
        let action2 = UIAlertAction(title: "アルバム", style: .default){
        (alert) in
        
        self.doAlbium()
        
        }
        
        let action3 = UIAlertAction(title: "キャンセル", style: .cancel)
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController,animated: true, completion:  nil)

        
        
    }
        
        
}
