//
//  ViewController.swift
//  Swift5FirebaseAnonymousLogin
//
//  Created by ikeda on 2020/08/06.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import Firebase


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    @IBAction func login(_ sender: Any) {
        
        Auth.auth().signInAnonymously{ (authResult, error) in
            
            let user  = authResult?.user
            print(user)
        
            //画面遷移
            let inputVC = self.storyboard?.instantiateViewController(identifier:"inputVC") as! inputViewController
            
            self.navigationController?.pushViewController(inputVC, animated: true)
            
            
        }
    
    }
    

}
