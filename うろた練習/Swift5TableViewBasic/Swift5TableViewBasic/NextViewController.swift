//
//  NextViewController.swift
//  Swift5TableViewBasic
//
//  Created by ikeda on 2020/06/25.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class NextViewController: UIViewController {

    var toDoString = String()
    
    @IBOutlet weak var todoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        todoLabel.text = toDoString
        
    }
    
}
