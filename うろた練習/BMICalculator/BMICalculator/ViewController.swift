//
//  ViewController.swift
//  BMICalculator
//
//  Created by ikeda on 2020/07/19.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var heightTextField: UITextField!
    
    @IBOutlet weak var weightTextField: UITextField!
    
    @IBOutlet weak var bmiLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        heightTextField.placeholder = "身長をcmで入力してください。"
        weightTextField.placeholder = "体重をkgで入力してください。"
        
    }

    func calculation(hight: Double, weight: Double) -> String {
         let h = hight / 100
         let w = weight
         var result = w / (h * h)
         result = floor(result * 10) / 10
         
         return result.description
    }
    
    @IBAction func calculationBtnAction(_ sender: Any) {
        
        let doubleH = Double(heightTextField.text!)
        let doubleW = Double(weightTextField.text!)
        bmiLabel.text = calculation(hight: doubleH!, weight: doubleW!)
        
    }
    
    

}

