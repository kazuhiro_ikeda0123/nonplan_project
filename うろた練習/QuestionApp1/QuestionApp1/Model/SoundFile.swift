//
//  SoundFile.swift
//  QuestionApp1
//
//  Created by ikeda on 2020/07/04.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import Foundation
import AVFoundation

class SoundFile{
    
    var player:AVAudioPlayer?
    
    func playSound(fileName:String,extentionName:String){
        
        
        //再生する
        let soundURL = Bundle.main.url(forResource: fileName, withExtension:extentionName)
        
        do {
            
            //効果音を鳴らす
            player = try AVAudioPlayer(contentsOf: soundURL!)
            player?.play()
        
        }catch  {
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
}














