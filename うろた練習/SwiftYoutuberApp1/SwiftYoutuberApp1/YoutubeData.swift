//
//  YoutubeData.swift
//  SwiftYoutuberApp1
//
//  Created by ikeda on 2020/08/26.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import Foundation

class YoutubeData {
    
    //取得したいものを仲介する　値を保持する
    
    var videoId:String = ""
    var publishedAt:String = ""
    var title:String = ""
    var imageURLString:String = ""
    var youtubeURL:String = ""
    var channelTitle:String = ""
    
}
