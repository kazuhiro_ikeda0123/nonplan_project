//
//  ViewController.swift
//  SwiftGoogleAdmob1
//
//  Created by ikeda on 2020/07/26.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import GoogleMobileAds



class ViewController: UIViewController {
    
    @IBOutlet weak var bannerView: GADBannerView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        bannerView.adUnitID = "ca-app-pub-2542441069562622/8808970370"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
    }


}

