//
//  NextViewController.swift
//  GamenSeniKakezan
//
//  Created by ikeda on 2020/06/28.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class NextViewController: UIViewController {
    
    @IBOutlet weak var anser: UILabel!
    
    @IBOutlet weak var box1: UITextField!
    
    @IBOutlet weak var box2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func button(_ sender: Any) {
        
        var nbox1 = box1.text!
        var nbox2 = box2.text!
        
        //var nnbox1 = Int(nbox1)!
        //var nnbox2 = Int(nbox2)!
        //var nanser = nnbox1 / nnbox2
        //anser.text = String(nanser)
        
        anser.text = String(Int(nbox1)! / Int(nbox2)!)
    }
}
