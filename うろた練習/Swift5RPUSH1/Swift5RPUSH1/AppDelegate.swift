//
//  AppDelegate.swift
//  Swift5RPUSH1
//
//  Created by ikeda on 2020/08/30.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import FirebaseMessaging
import UserNotifications
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var notificationGranted = true

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        FirebaseApp.configure()
        
        if #available(ios 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self as!
            UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound],
                                                                                  categories: nil)
            
            application.registerForRemoteNotifications()
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
                
                (granted, error) in
                self.notificationGranted = granted
                if let error = error {
                
                print("granted, but Error in notiFication premission:\(error.localizedDescription)")
                
                }
                
            }
            
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        if let messageID = userInfo["gcm.message_id"] {
            
            print("Message ID: \(messageID)")
            
        }
        
        print(userInfo)
        
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

@available(iOS 10, *)
    extention AppDelegate : UNUserNotificationCenterDelegate {
        
        func userNotificationCenter(_ center: UNUserNotificationCenter, willpresent notification: UNNotification, withCompletionHandler CompletionHandler: @escaping(UNNotificationActionOptions) -> Void) {
            
            let userInfo = notification.request.content.userInfo
            
            if let messageID = userInfo["gcm.message_id"] {
                
                print("Message ID \(messageID)")
                
            }
            
            print(userInfo)
            
            CompletionHandler([])
            
        }
        
        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler CompletionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        if let messageID = userInfo["gcm.message_id"] {
            
            print("MessageID")
            
        }
        
        print(userInfo)
        
        CompletionHandler()
        
        }
        
    }

}
