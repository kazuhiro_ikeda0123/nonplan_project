//
//  CustomCell.swift
//  ChatApp1
//
//  Created by ikeda on 2020/08/04.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var iconImageView: UIImageView!
        
    @IBOutlet weak var messageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
