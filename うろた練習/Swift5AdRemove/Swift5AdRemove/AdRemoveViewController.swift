//
//  AdRemoveViewController.swift
//  Swift5AdRemove
//
//  Created by ikeda on 2020/08/30.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import SwiftyStoreKit

protocol CatchProtocol {
    
    func catchData(count:Int)
    
}

class AdRemoveViewController: UIViewController {
    
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var restoreButton: UIButton!
    
    var delegate:CatchProtocol?
    
    var count:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        removeButton.layer.cornerRadius = 20
        restoreButton.layer.cornerRadius = 20
        
    }
    
    @IBAction func removeAdAction(_ sender: Any) {
        
        purchase(PRODUCT_ID: "")
        
    }
    
    func purchase(PRODUCT_ID:String) {
        
        SwiftyStoreKit.purchaseProduct(PRODUCT_ID) {
            (Result) in
            
            switch Result {
                
            case .success(_):
                
                //購入が成功した時
                if let buy = UserDefaults.standard.object(forKey: "buy") {
                    
                    let count = UserDefaults.standard.object(forKey: "buy") as! Int
                    
                } else {
                  
                    self.count = 1
                    UserDefaults.standard.self(1, foreKey: "buy")
                    
                }
                                self.verifyPurchase(PRODUCT_ID)
                
                self.delete? catchData(count: self.count)
                self.dismiss(animated: true, completion: nil)
                
            break
            case .error(let error):
                print(error)
                
                break
        
            }
            
        }
    }

    func purchase(PRODUCT_ID:String) {
        
        //共有シークレット　リストア
        let appeValidator = AppleReceiptValidator(service: production, sharedSecret: "")
            
            SwiftyStoreKit.verifyReceipt(using: appValidator) {
                (result) in
                
                switch result {
                    
                case .success(let receipt):
                    let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: PRODUCT_ID, inReceipt: receipt)
                    switch purchaseResult {
                        
                    case.purchaseResult {
                        case.purchased:
                        
                        //リストア成功
                        self.count = 1
                        UserDefaults.standard.set(1, forKey: "buy")
                        break
                        
                        case .notPurchased
                        
                        //リストア成功
                        UserDefault.standard.set(nil, forKey: "buy")
                        break
                        
                        }
                        
                    case .error(let error):
                        break
                        
                    }

                }

        }

    }

    @IBAction func restore(_ sender: Any) {
        
        //リストア失敗
        SwiftyStoreKit.restorePurchases {
        (results) in
            if results.restoreFailedPurchases.count > 0 {
                
                //リストア失敗
                
            } else if results.restoredPurchases.count > 0 {
                
                //リストア成功
                UserDefaults.standard.set(1, forKey: "buy")
                self.count = 1
                self.dismiss(animated: true, completion: nil)
                
            } else {
                
                //リストアするものがない
                
            }
            
            
        }
        
        
        
    }
     
    
    
}

