//
//  ViewController.swift
//  Swift5AdRemove
//
//  Created by ikeda on 2020/08/30.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ViewController: UIViewController,GADBannerViewDelegate {

    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //もしbuyというキー値に数字が入っていたら
        //購入済み = 広告を排除
        
        if let buy = UserDefaults.standard.object(forKey: "buy") {
            
            let count = UserDefaults.standard.object(forKey: "buy") as! Int
            
            if count == 1 {
                
                bannerView.removeFromSuperview()
                
            } else {
                    
                
            }
            
        } else {
            
            //広告を設定していく
            bannerView.adUnitID = "ca-app-pub-9800803408970740/6497040335"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            
        }
        
        
        
    }
    
    
    @IBAction func next(_ sender: Any) {
        
        performSegue(withIdentifier: "next", sender: nil)
        
        
        
        
        
        
    }
    
    
    

}
