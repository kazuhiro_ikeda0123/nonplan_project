//
//  AppDelegate.swift
//  Swift5AdRemove
//
//  Created by ikeda on 2020/08/30.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyStoreKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //GoogleMobileAdsを使えるようにする
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        //アプリ内課金のシステムを使えるようにする
        SwiftyStoreKit.completeTransactions(atomically: true) {
            
            purchases in for purchase in purchases {
                
                switch purchase.transaction.transactionState {
                    
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                     
                    }
            
                case .failed, .purchasing, .deferred:
                    break
                @unknown default:
                    fatalError()
                    
                }
                
            }
            
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

