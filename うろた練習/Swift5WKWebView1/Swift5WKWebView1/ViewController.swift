//
//  ViewController.swift
//  Swift5WKWebView1
//
//  Created by ikeda on 2020/06/24.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController,WKNavigationDelegate {
    
    var webView = WKWebView()
   
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - toolBar.frame.size.height * 3)
        view.addSubview(webView)
        webView.navigationDelegate = self
        
        
        webView.navigationDelegate = self
        let url = URL(string: "https://www.yahoo.co.jp/")
        let request = URLRequest(url: url!)
        webView.load(request)
        
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
        print("読み込み開始")
        indicator.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    
        print("ロード完了")
        indicator.stopAnimating()
    }
    @IBAction func go(_ sender: Any) {
        
        webView.goForward()
    }
    @IBAction func back(_ sender: Any) {
    
        webView.goBack()
    }
}


