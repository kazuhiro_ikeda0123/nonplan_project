//
//  ViewController.swift
//  hairetsu1-4
//
//  Created by ikeda on 2020/06/25.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    var count = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func button(_ sender: Any) {
        
        var array = ["1","2","3","4"]
    
        label.text = array[count]
    
        count = count + 1
        
        if count > 3 {
            count = 0
        }
    
    }
    
    
    
}

