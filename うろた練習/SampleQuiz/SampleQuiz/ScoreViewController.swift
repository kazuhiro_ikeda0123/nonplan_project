//
//  ScoreViewController.swift
//  SampleQuiz
//
//  Created by ikeda on 2020/07/15.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    
    var correct = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scoreLabel.text = "\(correct)問正解"
        
    }
    
    @IBAction func toTopBtnAction(_ sender: Any) {
        
        
       self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        
        
        
        
        
    }
    
    
    
    
}
