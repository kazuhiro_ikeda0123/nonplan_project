//
//  QuizViewController.swift
//  SampleQuiz
//
//  Created by ikeda on 2020/07/15.
//  Copyright © 2020 Swift.study.kazuhiro. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {
    
    @IBOutlet weak var quizNumberLabel: UILabel!
    
    @IBOutlet weak var quizTextView: UITextView!
    
    @IBOutlet weak var answerBtn1: UIButton!
    @IBOutlet weak var answerBtn2: UIButton!
    @IBOutlet weak var answerBtn3: UIButton!
    @IBOutlet weak var answerBtn4: UIButton!
    
    @IBOutlet weak var judgeImageView: UIImageView!
    
    var csvArray: [String] = []
    
    var quizArray: [String] = []
    
    var quizCount = 0
    
    var quizTotal = 5
    
    var correctCount = 0
    
    var explanetionBGView = UIView()
    var explanationBGX = 0.0
    
    var correctLabel = UILabel()
    
    var backBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let screenSize:CGSize = UIScreen.main.bounds.size
        explanationBGX = Double(screenSize.width/2) - 320/2
        explanetionBGView.backgroundColor = UIColor.lightGray
        explanetionBGView.frame = CGRect(x: explanationBGX,y:Double(screenSize.height), width: 320, height:  210)
        explanetionBGView.isUserInteractionEnabled = true
        self.view.addSubview(explanetionBGView)
        
        correctLabel.frame = CGRect(x: 60, y:  100, width: 300, height: 30)
        correctLabel.font = UIFont.systemFont(ofSize: 20)
        explanetionBGView.addSubview(correctLabel)
        
        backBtn.frame = CGRect(x: 90, y: 200, width: 132, height: 37)
        backBtn.backgroundColor = UIColor.white
        backBtn.tintColor = UIColor.white
        backBtn.setTitle("戻る", for: .normal)
        backBtn.setTitleColor(UIColor.black, for: .normal)
        backBtn.addTarget(self, action: #selector(QuizViewController.backBtnTapped), for: UIControl.Event.touchUpInside)
        explanetionBGView.addSubview(backBtn)
        
        csvArray = loadCSV(fileName: "quiz")
        print(csvArray)
        
        quizArray = csvArray[quizCount].components(separatedBy: ",")
        quizNumberLabel.text = "第\(quizCount + 1)問"
        quizTextView.text = quizArray[0]
        answerBtn1.setTitle(quizArray[2], for: .normal)
        answerBtn2.setTitle(quizArray[3], for: .normal)
        answerBtn3.setTitle(quizArray[4], for: .normal)
        answerBtn4.setTitle(quizArray[5], for: .normal)
      
    }
    
    @objc func backBtnTapped(){
        let screenHeight = Double(UIScreen.main.bounds.size.height)
        UIView.animate(withDuration: 0.5, animations: {() -> Void in self.explanetionBGView.frame = CGRect(x: self.explanationBGX, y: screenHeight, width: 320, height: 210)
        })

        answerBtn1.isEnabled = true
        answerBtn2.isEnabled = true
        answerBtn3.isEnabled = true
        answerBtn4.isEnabled = true
        judgeImageView.isHidden = true
        nextQuiz()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let sVC = segue.destination as! ScoreViewController
        sVC.correct = correctCount
    }
    
    func loadCSV(fileName: String) -> [String] {
        let csvBundle = Bundle.main.path(forResource: fileName, ofType: "csv")!
        do {
            let csvData = try String(contentsOfFile: csvBundle,encoding: String.Encoding.utf8)
            let lineChange = csvData.replacingOccurrences(of: "\r", with: "\n")
            csvArray = lineChange.components(separatedBy: "\n")
        } catch {
            print("エラー")
        }
        return csvArray
    }
    
    @IBAction func btnAction(_ sender: Any) {
        
        if (sender as AnyObject).tag == Int(quizArray[1]) {
            correctCount += 1
              judgeImageView.image = UIImage(named: "correct")
                    } else {
                        judgeImageView.image = UIImage(named: "incorrect")
                    }
                    judgeImageView.isHidden = false
                    explanation()
                }
    
    func explanation() {
        
        let correctNumber = Int(quizArray[1])!
        correctLabel.text = "答え：\(quizArray[correctNumber + 2])"
        
        let answerBtnY = answerBtn1.frame.origin.y
        UIView.animate(withDuration: 0.5, animations: {() -> Void in self.explanetionBGView.frame = CGRect(x: self.explanationBGX, y: Double(answerBtnY), width: 320, height: 280)
        })
        answerBtn1.isEnabled = false
        answerBtn2.isEnabled = false
        answerBtn3.isEnabled = false
        answerBtn4.isEnabled = false
    }
    
    func nextQuiz() {
             quizCount += 1
             quizArray.removeAll()
        
        if quizCount < quizTotal{
        
             quizArray = csvArray[quizCount].components(separatedBy: ",")
             quizNumberLabel.text = "第\(quizCount + 1)問"
             quizTextView.text = quizArray[0]
             answerBtn1.setTitle(quizArray[2], for: .normal)
             answerBtn2.setTitle(quizArray[3], for: .normal)
             answerBtn3.setTitle(quizArray[4], for: .normal)
             answerBtn4.setTitle(quizArray[5], for: .normal)
            
            } else {
                performSegue(withIdentifier: "score" , sender: nil)
    
    
        
        
    }
    
    
    
}

}
